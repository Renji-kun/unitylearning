﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FiniteStateMachine
{
    public class Pawn : MonoBehaviour
    {
        private Rigidbody _rbody;
        private CapsuleCollider _collider;
        private bool wasCrouched;
        private float prevCameraHeight;

        public float Speed;
        public float JumpHeight;
        public float DiveVelocity;

        public float CrouchHeight;
        public float CameraCrouchHeight;

        public Transform CameraObject;




        // Use this for initialization
        void Start()
        {
            _rbody = GetComponent<Rigidbody>();
            _collider = GetComponent<CapsuleCollider>();
            _rbody.freezeRotation = true;
        }

        /// <summary>
        /// Check character to be on the ground.
        /// </summary>
        /// <returns><c>true</c>, if character is grounded, <c>false</c> character isn`t grounded.</returns>
        public bool IsGrounded()
        {
            return Physics.Raycast(transform.position, -transform.up, 1.0f);
        }

        /// <summary>
        /// Move the player at specified direction and speed.
        /// </summary>
        /// <param name="direction">Direction vector.</param>
        public void Move(Vector3 direction)
        {
            transform.Translate(direction * Speed * Time.deltaTime);
        }

        public void Jump()
        {
            _rbody.AddForce(transform.up * JumpHeight, ForceMode.Impulse);
            //rbody.velocity += transform.up * JumpVelocity * Time.deltaTime;
        }

        public void Dive()
        {
            _rbody.AddForce(-transform.up * DiveVelocity, ForceMode.Impulse);
        }


        private float CalculateJumpVerticalSpeed()
        {
            return Mathf.Sqrt(2 * JumpHeight * Physics.gravity.y);
        }

        public void Crouch()
        {
            if(!wasCrouched)
            {
                prevCameraHeight = CameraObject.position.y;
                wasCrouched = true;
            }
            CameraObject.position = Vector3.Lerp(CameraObject.position, new Vector3(CameraObject.position.x, CameraCrouchHeight, CameraObject.position.z), Time.deltaTime * 5f);
        }

        public void Uncrouch()
        {
            wasCrouched = false;
            //CameraObject.position = Vector3.Lerp(CameraObject.position, new Vector3(CameraObject.position.x, prevCameraHeight, CameraObject.position.z), Time.deltaTime * 5f);
            CameraObject.position = new Vector3(CameraObject.position.x, prevCameraHeight, CameraObject.position.z);
        }
    }
}


