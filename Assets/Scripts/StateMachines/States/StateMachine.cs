﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FiniteStateMachine
{
    public enum EState
    {
        Idle = 0,
        Moving = 1,
        Jumping = 2,
        Crouching = 3,
        Diving = 4,
        Falling = 5
    }

    public class StateMachine : MonoBehaviour
    {
        public Dictionary<EState, StateBase> states;
        public StateBase currentState
        {
            get { return _currentState; }
        }

        public Pawn Owned_Pawn
        {
            get { return _pawn; }
        }

        public Controller Owned_Controller
        {
            get { return _controller; }
        }

        protected StateBase _currentState;
        protected Pawn _pawn;
        protected Controller _controller;

        private void Start()
        {
            _pawn = GetComponent<Pawn>();
            _controller = GetComponent<Controller>();
            InitializeStates();
        }

        /// <summary>
        /// Initializes the machine states.
        /// </summary>
        public void InitializeStates()
        {
            states = new Dictionary<EState, StateBase>();
            states.Add(EState.Idle, new IdleState(this));
            states.Add(EState.Moving, new MovingState(this));
            states.Add(EState.Jumping, new JumpState(this));
            states.Add(EState.Diving, new DiveState(this));
            states.Add(EState.Crouching, new CrouchState(this));
            states.Add(EState.Falling, new FallingState(this));
            _currentState = states[EState.Idle];
        }

        /// <summary>
        /// Change the current state to desirable state
        /// </summary>
        /// <param name="state">Desirable state</param>
        public void GoToState(EState state)
        {
            _currentState.StateExit();
            _currentState = states[state];
            _currentState.StateEnter();
        }

        void FixedUpdate()
        {
            _currentState.HandleInput();
            _currentState.UpdateState();
        }
    }
}


