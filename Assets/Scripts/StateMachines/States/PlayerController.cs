﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FiniteStateMachine
{
    public class PlayerController : Controller
    {
        public override bool IsMoving() 
        {
            return (Input.GetAxis("Horizontal") != 0) || (Input.GetAxis("Vertical") != 0);
        }

        public override bool RequestCrouch()
        {
            return Input.GetButton("Fire1");
        }

        public override bool RequestJump()
        {
            return Input.GetButton("Jump");
        }

        public override bool RequestDive()
        {
            return Input.GetButton("Fire2");
        }

        public override Vector3 GetMovementDirection()
        {
            return new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        }
    }
}


