﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FiniteStateMachine
{
    public class CrouchState : StateBase
    {
        public CrouchState(StateMachine owner) : base(owner) { }

        public override void HandleInput()
        {
            if (!_owner.Owned_Controller.RequestCrouch())
                _owner.GoToState(EState.Idle);
        }

        public override void UpdateState()
        {
            Debug.Log("Crouching");
            //_controller.Crouch();
            _owner.Owned_Pawn.Crouch();
        }

        public override void StateExit()
        {
            _owner.Owned_Pawn.Uncrouch();
        }
    }

}

