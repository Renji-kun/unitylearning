﻿using UnityEngine;

namespace FiniteStateMachine
{
    public abstract class StateBase
    {
        protected StateMachine _owner;

        public StateBase(StateMachine owner) { _owner = owner; }

        public virtual void HandleInput() { } 

        public virtual void StateEnter() { }

        public virtual void UpdateState() { }

        public virtual void StateExit() { }
    }
}


