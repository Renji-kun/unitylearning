﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FiniteStateMachine
{
    public class IdleState : StateBase
    {
        public IdleState(StateMachine owner) : base(owner) { }

        public override void HandleInput()
        {
            if (!_owner.Owned_Pawn.IsGrounded())
                _owner.GoToState(EState.Falling);
            else
            {
                if (_owner.Owned_Controller.IsMoving())
                    _owner.GoToState(EState.Moving);
                else if (_owner.Owned_Controller.RequestJump())
                    _owner.GoToState(EState.Jumping);
                else if (_owner.Owned_Controller.RequestCrouch())
                    _owner.GoToState(EState.Crouching);
            }
        }

        public override void UpdateState()
        {
            Debug.Log("Idle");
        }

        public override void StateExit()
        {
            Debug.Log("Exit Idle state");
        }
    }
}


