﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FiniteStateMachine
{
    public class DiveState : StateBase
    {
        public DiveState(StateMachine owner) : base(owner) { }

        public override void UpdateState()
        {
            Debug.Log("Diving");
            //_controller.Dive();
            if (!_owner.Owned_Pawn.IsGrounded())
                _owner.Owned_Pawn.Dive();
            else
                _owner.GoToState(EState.Idle);
            //_owner.GoToState(EState.Falling);
        }
    }
}
