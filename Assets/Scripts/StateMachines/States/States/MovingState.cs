﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FiniteStateMachine
{
    public class MovingState : StateBase
    {
        public MovingState(StateMachine owner) : base(owner) { }

        public override void HandleInput()
        {
            if (!_owner.Owned_Pawn.IsGrounded())
                _owner.GoToState(EState.Falling);
            else
                if (!_owner.Owned_Controller.IsMoving())
                    _owner.GoToState(EState.Idle);
                else if (_owner.Owned_Controller.RequestJump())
                    _owner.GoToState(EState.Jumping);
        }

        public override void UpdateState()
        {
            Debug.Log("Moving");
            //_controller.Move(dir);
            _owner.Owned_Pawn.Move(_owner.Owned_Controller.GetMovementDirection());
        }

        public override void StateExit()
        {
            Debug.Log("Exit Moving State");
        }
    }

}

