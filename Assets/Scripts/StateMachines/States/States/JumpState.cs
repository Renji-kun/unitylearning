﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FiniteStateMachine
{
    public class JumpState : StateBase
    {
        public JumpState(StateMachine owner) : base(owner) { }



        public override void StateEnter()
        {
            Debug.Log("Jumping");
            _owner.Owned_Pawn.Jump();
            _owner.GoToState(EState.Falling);
        }

        public override void StateExit()
        {
            Debug.Log("Exit Jumping state");
        }
    }

}

