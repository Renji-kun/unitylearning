﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FiniteStateMachine
{
    public class FallingState : StateBase
    {
        public FallingState(StateMachine owner) : base(owner) { }

        public override void HandleInput()
        {
            if (_owner.Owned_Controller.RequestDive())
                _owner.GoToState(EState.Diving);
        }

        public override void UpdateState()
        {
            Debug.Log("Falling");
            if (_owner.Owned_Pawn.IsGrounded())
                _owner.GoToState(EState.Idle);
        }
    }
}


