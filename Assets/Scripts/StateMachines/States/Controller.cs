﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FiniteStateMachine
{
    public abstract class Controller : MonoBehaviour
    {
        protected Pawn _pawn;

        protected void Start()
        {
            _pawn = GetComponent<Pawn>();
        }

        public virtual bool IsMoving() { return false; }

        public virtual bool RequestJump() { return false; }

        public virtual bool RequestDive() { return false; }

        public virtual bool RequestCrouch() { return false; }

        public virtual Vector3 GetMovementDirection() { return Vector3.zero; }

    }
}

