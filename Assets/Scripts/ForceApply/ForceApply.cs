﻿using UnityEngine;
using UnityEngine.EventSystems;


public class ForceApply : MonoBehaviour, IPointerDownHandler
{
    public float ForceValue;

    private Rigidbody rbody;
    
	void Awake ()
    {
        rbody = GetComponent<Rigidbody>();
        print("Awake");	
	}

    public void OnPointerDown(PointerEventData data)
    {
        Vector3 forceDirection = (data.pointerCurrentRaycast.worldPosition - Camera.main.transform.position);
        print(forceDirection);
        //rbody.AddForce(forceDirection.normalized * ForceValue);
        rbody.AddForceAtPosition(forceDirection * ForceValue, data.pointerCurrentRaycast.worldPosition);
        //print("Touched object");
    }
}
