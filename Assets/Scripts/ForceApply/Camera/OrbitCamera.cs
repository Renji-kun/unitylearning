﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCamera : MonoBehaviour {

    public float turnSpeed = 150f;
    public Transform target;

    private void Start()
    {
        transform.LookAt(target);
    }

    // Update is called once per frame
    void Update ()
    {
	    if(Input.GetAxis("Horizontal")!=0)
        {
            transform.RotateAround(target.position, Vector3.up, Input.GetAxis("Horizontal")*turnSpeed*Time.deltaTime);
        }
	}
}
